import React from "react";

const Filtrer = ({ filtrerTitre, filtrerNote, setFiltrerTitre, setFiltrerNote }) => {
  return (
    <div className="flex flex-col md:flex-row justify-between mb-4">
      <input
        type="text"
        placeholder="Filtrer par titre"
        value={filtrerTitre}
        onChange={(e) => setFiltrerTitre(e.target.value)}
        className="border p-2 rounded mb-2 md:mb-0 md:mr-2"
      />
      <input
        type="number"
        placeholder="Filter par note"
        value={filtrerNote}
        onChange={(e) => setFiltrerNote(e.target.value)}
        className="border p-2 rounded"
      />
    </div>
  );
};

export default Filtrer;
