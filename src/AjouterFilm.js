import React, { useState } from "react";

const AjouterFilm = ({ addFilm }) => {
  const [titre, setTitre] = useState("");
  const [description, setDescription] = useState("");
  const [lienURL, setLienURL] = useState("");
  const [note, setNote] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    addFilm({ titre, description, lienURL, note });
    setTitre("");
    setDescription("");
    setLienURL("");
    setNote("");
  };

  return (
    <form onSubmit={handleSubmit} className="mb-4">
      <div className="flex flex-col mb-2">
        <label className="mb-1">Title:</label>
        <input
          type="text"
          value={titre}
          onChange={(e) => setTitre(e.target.value)}
          className="border p-2 rounded"
        />
      </div>
      <div className="flex flex-col mb-2">
        <label className="mb-1">Description:</label>
        <textarea
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          className="border p-2 rounded"
        />
      </div>
      <div className="flex flex-col mb-2">
        <label className="mb-1">Lien URL:</label>
        <input
          type="text"
          value={lienURL}
          onChange={(e) => setLienURL(e.target.value)}
          className="border p-2 rounded"
        />
      </div>
      <div className="flex flex-col mb-4">
        <label className="mb-1">Note:</label>
        <input
          type="number"
          value={note}
          onChange={(e) => setNote(e.target.value)}
          className="border p-2 rounded"
        />
      </div>
      <button
        type="submit"
        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
      >
        Add Movie
      </button>
    </form>
  );
};

export default AjouterFilm;
