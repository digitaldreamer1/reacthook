import React, { useState } from "react";
import FilmList from "./FilmList";
import Filtrer from "./Filtrer";
import AjouterFilm from "./AjouterFilm";
import "./index.css";

function App() {
  const [films, setFilms] = useState([
    {
      titre: "Inception",
      description: "Un voleur qui vole des secrets d'entreprise en utilisant la technologie de partage de rêves.",
      posterURL: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQXWnBnPN47nWvqWJAxw-vmchKc_2u1zkG6Bw&s",
      note: 9,
    },
    {
      titre: "Interstellar",
      description: "Une équipe d'explorateurs voyage à travers un trou de ver dans l'espace pour assurer la survie de l'humanité.",
      posterURL: "https://m.media-amazon.com/images/I/91vIHsL-zjL._AC_UF1000,1000_QL80_.jpg",
      note: 8.5,
    },
  ]);

  const [filtrerTitre, setFiltrerTitre] = useState("");
  const [filtrerNote, setFiltrerNote] = useState("");

  const ajouterFilm = (film) => {
    setFilms([...films, film]);
  };

  const filtrerFilms = films.filter(
    (film) =>
      film.titre.toLowerCase().includes(filtrerTitre.toLowerCase()) &&
      film.note >= (filtrerNote ? parseFloat(filtrerNote) : 0)
  );

  return (
    <div className="App">
      <header className="bg-gray-800 text-white text-center py-4">
        <h1 className="text-2xl font-bold">Cinema App</h1>
      </header>
      <main className="p-8">
        <Filtrer
          filtrerTitre={filtrerTitre}
          filtrerNote={filtrerNote}
          setFiltrerTitre={setFiltrerTitre}
          setFiltrerNote={setFiltrerNote}
        />
        <AjouterFilm ajouterFilm={ajouterFilm} />
        <FilmList films={filtrerFilms} />
      </main>
    </div>
  );
}

export default App;
