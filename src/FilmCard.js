import React from "react";

const FilmCard = ({ titre, description, posterURL, note }) => {
  return (
    <div className="max-w-sm bg-white shadow-lg rounded-lg overflow-hidden m-4">
      <img className="w-full h-64 object-cover" src={posterURL} alt={titre} />
      <div className="p-4">
        <h2 className="text-xl font-bold">{titre}</h2>
        <p className="text-gray-600">{description}</p>
        <p className="text-gray-800">Note: {note}</p>
      </div>
    </div>
  );
};

export default FilmCard;
