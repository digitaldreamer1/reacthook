import React from "react";
import FilmCard from "./FilmCard";

const FilmList = ({ films }) => {
  return (
    <div className="flex flex-wrap justify-center">
      {films.map((film, index) => (
        <FilmCard key={index} {...film} />
      ))}
    </div>
  );
};

export default FilmList;
